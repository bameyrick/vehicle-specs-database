import { Sequelize as _Sequelize, ISequelizeConfig, IBuildOptions, Model as _Model } from 'sequelize-typescript';
import { Manufacturer } from './models/manufacturer';
import { Model } from './models/model';
import { ModelVariant } from './models/model-variant';
import { Trim } from './models/trim';
import { TrimEngine } from './models/trim-engine';
import { TrimVariant } from './models/trim-variant';

export const Sequelize = new _Sequelize(<ISequelizeConfig>{
	database: 'vehicle_specs',
	dialect: 'postgres',
	host: '127.0.0.1',
	username: 'vehicle_specs_service',
	password: process.env.DB_PASSWORD,
	logging: false,
	pool: {
		max: 1000,
		idle: 100000,
		acquire: 100000,
	},
});

Sequelize.addModels([Manufacturer, Model, ModelVariant, Trim, TrimEngine, TrimVariant]);

interface ModelClass<T> {
	new (values?: any, options?: IBuildOptions): T;
}

export function GetModel<T>(modelName: string): ModelClass<T> & typeof _Model {
	return Sequelize._[modelName] as any;
}

export { Manufacturer } from './models/manufacturer';
export { Model } from './models/model';
export { ModelVariant } from './models/model-variant';
export { Trim } from './models/trim';
export { TrimEngine } from './models/trim-engine';
export { TrimVariant } from './models/trim-variant';
export { EDrivetrain, EFuelType, ETransmission } from './enums';
