export enum EFuelType {
	Petrol,
	PetrolLPG,
	Diesel,
	Electric,
	PetrolPlugIn,
	PetrolHybrid,
	DieselHybrid,
	DieselPlugIn,
	Ethanol,
	PetrolCNG,
}
