export enum EDrivetrain {
	RearWheelDrive = 'RWD',
	FrontWheelDrive = 'FWD',
	FourWheelDrive = '4WD',
	FourWheelSteering = '4WS',
}
