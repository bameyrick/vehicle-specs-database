import { Table, Model as _Model, Column, HasMany, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { TrimEngine } from './trim-engine';
import { ModelVariant } from './model-variant';

@Table
export class Trim extends _Model<Trim> {
	@Column name: string;

	@ForeignKey(() => ModelVariant)
	@Column
	model_variant_id: number;

	@BelongsTo(() => ModelVariant)
	model_variant: ModelVariant;

	@HasMany(() => TrimEngine)
	engines: TrimEngine[];
}
