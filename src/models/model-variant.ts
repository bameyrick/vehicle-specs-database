import { Table, Model as _Model, Column, HasMany, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { Trim } from './trim';
import { Model } from './model';

@Table
export class ModelVariant extends _Model<ModelVariant> {
	@Column name: string;

	@Column start_year?: number;

	@Column end_year?: number;

	@Column generation_start_year?: number;

	@Column generation_end_year?: number;

	@ForeignKey(() => Model)
	@Column
	model_id: number;

	@BelongsTo(() => Model)
	model: Model;

	@HasMany(() => Trim)
	trims: Trim[];
}
