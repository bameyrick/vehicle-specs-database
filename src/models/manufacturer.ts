import { Table, Model as _Model, Column, HasMany } from 'sequelize-typescript';
import { Model } from './model';

@Table
export class Manufacturer extends _Model<Manufacturer> {
	@Column name: string;

	@HasMany(() => Model)
	models: Model[];
}
