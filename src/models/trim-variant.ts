import { Table, Model as _Model, Column, BelongsTo, ForeignKey } from 'sequelize-typescript';
import { EDrivetrain, EFuelType, ETransmission } from '../enums';
import { TrimEngine } from './trim-engine';

@Table
export class TrimVariant extends _Model<TrimVariant> {
	@Column name: string;

	@Column start_year?: number;

	@Column end_year?: number;

	@Column acceleration?: number;

	@Column top_speed?: number;

	@Column power?: number;

	@Column torque_nm?: number;

	@Column torque_lbs?: number;

	@Column miles_per_tank?: number;

	@Column euro_emissions_standard?: number;

	@Column drive_train?: EDrivetrain;

	@Column engine_size?: number;

	@Column cylinders?: number;

	@Column fuel_type?: EFuelType;

	@Column gearbox?: string;

	@Column transmission?: ETransmission;

	@Column valves?: number;

	@Column fuel_capacity?: number;

	@Column height?: number;

	@Column width?: number;

	@Column length?: number;

	@Column wheelbase?: number;

	@Column weight?: number;

	@Column turning_circle?: number;

	@Column doors?: number;

	@Column seats?: number;

	@Column luggage_capacity?: number;

	@Column unbraked_towing_weight?: number;

	@Column braked_towing_weight?: number;

	@Column mpg?: number;

	@Column insurance_group?: number;

	@ForeignKey(() => TrimEngine)
	@Column
	trim_engine_id: number;

	@BelongsTo(() => TrimEngine)
	trim_engine: TrimEngine;
}
