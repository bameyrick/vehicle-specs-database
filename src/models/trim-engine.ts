import { Table, Model as _Model, Column, HasMany, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { TrimVariant } from './trim-variant';
import { Trim } from './trim';

@Table
export class TrimEngine extends _Model<TrimEngine> {
	@Column name: string;

	@ForeignKey(() => Trim)
	@Column
	trim_id: number;

	@BelongsTo(() => Trim)
	trim: Trim;

	@HasMany(() => TrimVariant)
	trim_variants: TrimVariant[];
}
