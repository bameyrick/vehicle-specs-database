import { Table, Model as _Model, Column, HasMany, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { ModelVariant } from './model-variant';
import { Manufacturer } from './manufacturer';

@Table
export class Model extends _Model<Model> {
	@Column name: string;

	@ForeignKey(() => Manufacturer)
	@Column
	manufacturer_id: number;

	@BelongsTo(() => Manufacturer)
	manufacturer: Manufacturer;

	@HasMany(() => ModelVariant)
	variants: ModelVariant[];
}
